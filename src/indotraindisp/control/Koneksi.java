/*
 *TCI Easy Secure
 *Koneksi ke database
 */
package indotraindisp.control;
import indotraindisp.model.fungsi;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.ucanaccess.jdbc.*;
 
/**
 *
 */
public class Koneksi {
    private fungsi func  = new fungsi();
    int seq = 1;
    public static Koneksi db;
    private Connection connection;
 
    public Connection getConnection() {
        return connection;
    }
    //url = "jdbc:mysql://localhost/itd_server1";
    //user = "root";
    //password = "";
    public void dbConnection() { 
        int x = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url, password, path;
            path = func.getPath()+"/data/db.itd";
            System.out.println(path);
            url = "jdbc:ucanaccess://"+path; //file DB access
            password = "itdprodb2020";//pw user db
            connection = DriverManager.getConnection(url,"",password);//Koneksikan
            System.out.println("Terkoneksi ke DB Access");
        } catch (SQLException se) {
            System.out.println(se);
        } catch (ClassNotFoundException ex) {
            db = this;
            JOptionPane.showMessageDialog(null, "Aplikasi Error , Silahkan reinstall.", "Error", 0);
            System.exit(0);
        }
        db = this;
    }
 
    public static void main(String[] kon) {
        new Koneksi().dbConnection();
    }
}