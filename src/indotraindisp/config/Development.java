package indotraindisp.config;

import java.util.HashMap;
import java.util.Map;

public class Development {
    Map<String,Object> config = new HashMap<String,Object>();
    
    public Development(){
        config.put("ITSS_WEBSOCKET_HOST", "ws://localhost");
        config.put("ITSS_WEBSOCKET_PORT", "8031");
        config.put("ITSS_AES256_ENCRYPTION_KEY", Credentials.ITSS_AES256_KEY);
    }
}
