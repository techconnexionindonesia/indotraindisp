package indotraindisp.config;

import java.util.HashMap;
import java.util.Map;

public class Production {
    Map<String,Object> config = new HashMap<String,Object>();
    
    public Production(){
        config.put("ITSS_WEBSOCKET_HOST", "ws://production.example.com");
        config.put("ITSS_WEBSOCKET_PORT", "8031");
        config.put("ITSS_AES256_ENCRYPTION_KEY", Credentials.ITSS_AES256_KEY);
    }
}
