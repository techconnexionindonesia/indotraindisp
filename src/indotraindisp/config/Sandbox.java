package indotraindisp.config;

import java.util.HashMap;
import java.util.Map;

public class Sandbox {
    Map<String,Object> config = new HashMap<String,Object>();
    
    public Sandbox(){
        config.put("ITSS_WEBSOCKET_HOST", "ws://sandbox.example.com");
        config.put("ITSS_WEBSOCKET_PORT", "8031");
        config.put("ITSS_AES256_ENCRYPTION_KEY", Credentials.ITSS_AES256_KEY);
    }
}
