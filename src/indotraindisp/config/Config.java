package indotraindisp.config;

import java.util.HashMap;
import java.util.Map;

public class Config {
    String env = "development"; // Development , Sandbox , Production
    Map<String,Object> config = new HashMap<String,Object>();
    Development development_env = new Development();
    Sandbox sandbox_env = new Sandbox();
    Production production_env = new Production();
    
    public Config(){
        if (env.equals("development")) config = development_env.config;
        else if (env.equals("sandbox")) config = sandbox_env.config;
        if (env.equals("production")) config = production_env.config;
    }
    
    public Object getVar(String var){
        return config.get(var);
    }
}
