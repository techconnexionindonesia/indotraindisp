package indotraindisp.model;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Encryption {
	;
	public indotraindisp.config.Config config = new indotraindisp.config.Config();
	final String ENCRYPTION_KEY = (String) config.getVar("ITSS_AES256_ENCRYPTION_KEY");
	
	public String encrypt(String src) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, makeKey());
			String result = Base64.getEncoder().encode(cipher.doFinal(src.getBytes())).toString();
                        System.out.println(result);
                        return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String decrypt(String src) {
		String decrypted = "";
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, makeKey());
			decrypted = new String(cipher.doFinal(Base64.getDecoder().decode(src)));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
                System.out.println(decrypted);
		return decrypted;
	}
	
	Key makeKey() {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] key = md.digest(ENCRYPTION_KEY.getBytes("UTF-8"));
			return new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}