/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indotraindisp.model;

import indotraindisp.model.language.en;
import indotraindisp.model.language.id;
import indotraindisp.model.constants;
import indotraindisp.control.Koneksi;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author JAPAN RAILWAY
 */
public class fungsi {
        public String lang = "en";
        public static fungsi model;
        private en lang_en = new en();
        private id lang_id = new id();
        private constants constants = new constants();
        private Koneksi db;
        private Map<String, String> texts;
        private Map<String, String> cons_var = constants.Constants;
    
        //fungsi konvert nomor bulan ke judul bulan
        public String numberToMonth(String M){
        String hasil = null;
        if (null != M) switch (M) {
            case "01":
                hasil = "Januari";
                break;
            case "02":
                hasil = "Februari";
                break;
            case "03":
                hasil = "Maret";
                break;
            case "04":
                hasil = "April";
                break;
            case "05":
                hasil = "Mei";
                break;
            case "06":
                hasil = "Juni";
                break;
            case "07":
                hasil = "Juli";
                break;
            case "08":
                hasil = "Agustus";
                break;
            case "09":
                hasil = "September";
                break;
            case "10":
                hasil = "Oktober";
                break;
            case "11":
               hasil = "November";
               break;
            case "12":
                hasil = "Desember";
                break;
            default:
                break;
        }
        return hasil;
    }
        
        public String monthToNumber(String m){
            String hasil = null;
            if (null != m) switch (m) {
                case "Januari":
                    hasil = "01";
                    break;
                case "Februari":
                    hasil = "02";
                    break;
                case "Maret":
                    hasil = "03";
                    break;
                case "April":
                    hasil = "04";
                    break;
                case "Mei":
                    hasil = "05";
                    break;
                case "Juni":
                    hasil = "06";
                    break;
                case "Juli":
                    hasil = "07";
                    break;
                case "Agustus":
                    hasil = "08";
                    break;
                case "September":
                    hasil = "09";
                    break;
                case "Oktober":
                    hasil = "10";
                    break;
                case "November":
                    hasil = "11";
                    break;
                case "Desember":
                    hasil = "12";
                    break;
                default:
                    break;
            }
            return hasil;
        }
        
         public String getPath(){
        try {
            Process process = Runtime.getRuntime().exec("reg query " + 
                    '"'+ "HKEY_CURRENT_USER\\Software\\TCIPRO\\itd_game" + "\" /v " + "main_path");
            StreamReader reader = new StreamReader(process.getInputStream());
            reader.start();
            process.waitFor();
            reader.join();
            String output = reader.getResult();
            // Parse out the value
            String[] parsed = output.split("\t");
            return parsed[parsed.length-1].trim().substring(71);
        }
        catch (IOException | InterruptedException e) {
            return null;
        }

    }

    static class StreamReader extends Thread {
        private final InputStream is;
        private final StringWriter sw= new StringWriter();

        public StreamReader(InputStream is) {
            this.is = is;
        }

        @Override
        public void run() {
            try {
                int c;
                while ((c = is.read()) != -1)
                    sw.write(c);
            }
            catch (IOException e) { 
        }
        }

        public String getResult() {
            return sw.toString();
        }
    }
        
    public String cvt2Digit(int num){
        String n = String.valueOf(num);
        if (n.length()==1){
            n = "0"+n;
        }
        return n;
    }
    
    public String getStation(String kode){
        String result = "";
            switch (kode) {
                case "AK":
                    result = "Angke";
                    break;
                case "BJD":
                    result = "Bojong Gede";
                    break;
                case "BOO":
                    result = "Bogor";
                    break;
                case "CCR":
                    result = "Cicurug";
                    break;
                case "CJ":
                    result = "Cijerah";
                    break;
                case "CSA":
                    result = "Cisaat";
                    break;
                case "DP":
                    result = "Depok";
                    break;
                case "DU":
                    result = "Duri";
                    break;
                case "GMR":
                    result = "Gambir";
                    break;
                case "JAKK":
                    result = "Jakarta Kota";
                    break;
                case "JNG":
                    result = "Jatinegara";
                    break;
                case "KPB":
                    result = "Kampung Bandan";
                    break;
                case "MRI":
                    result = "Manggarai";
                    break;
                case "NMO":
                    result = "Nambo";
                    break;
                case "THB":
                    result = "Tanah Abang";
                    break;
                default:
                    break;
                case "SI":
                    result = "Sukabumi";
                    break;
            }
            return result;
    }
    
    public String getKeteranganWaktu(String delay){
        String result = "";
            switch (delay) {
                case "0":
                    result = "Tepat Waktu";
                    break;
                case "+1":
                    result = "Terlambat 1 Menit";
                    break;
                case "+2":
                    result = "Terlambat 2 menit";
                    break;
                case "+3":
                    result = "Terlambat 3 menit";
                    break;
                case "-1":
                    result = "Awal 1 menit";
                    break;
                case "-2":
                    result = "Awal 2 menit";
                    break;
                default:
                    break;
            }
            return result;
    }
    
    public String cvtTotalHours(String hours){
        String[] selection = hours.split(":");
        String result = selection[0]+" "+text("hour")+" "+selection[1]+" "+text("minute");
        return result;
    }
    
    public String getAddOneMinute(String hours){
        String[] selection = hours.split(":");
        int hour = Integer.valueOf(selection[0]);
        int minute = Integer.valueOf(selection[1]);
        if (minute == 59){
            hour++;
            minute = 0;
        }else minute++;
        String result = cvt2Digit(hour)+":"+cvt2Digit(minute);
        return result;
    }
    
    public void setLang(String language){
        lang = language;
        if (language.equals("en")){
            texts = lang_en.text;
        } else {
            texts = lang_id.text;
        }
        System.out.println(texts);
        model = this;
    }
    
    public void importConnection(Koneksi conn){
        db = conn;
    }
    
    public String text(String text){
        return texts.get(text);
    }
    
    public String var(String cons){
        return cons_var.get(cons);
    }
    
    public void setCenter(JFrame frame){
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
    }
    
    public void changeLang(String lang) {
        Map<String,String> criteria = new HashMap<String,String>();
        criteria.put("language",lang);
        dbUpdate(criteria, "main");
        setLang(lang);
    }
    
    /*------ Database Model -------*/
    public ResultSet dbGet(Map<String, String> criteria, String table){
        ResultSet result = null;
        try {
            String sql = "SELECT * ";
            sql += "FROM "+table+" ";
            sql += "WHERE ";
            String criteriaQuery = "";
            for (String key: criteria.keySet()){
                if (!"".equals(criteriaQuery)){
                    criteriaQuery += " AND ";
                }
                criteriaQuery += key+"='"+criteria.get(key)+"'";
            }
            sql += criteriaQuery;
            Statement st = db.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            result = rs;
        } catch (SQLException ex) {
            Logger.getLogger(fungsi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public ResultSet dbGet(String table){
        ResultSet result = null;
        try {
            String sql = "SELECT * ";
            sql += "FROM "+table;
            Statement st = db.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            result = rs;
        } catch (SQLException ex) {
            Logger.getLogger(fungsi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public int dbUpdate(Map<String,String> criteria,String table){
        int result = 0;
        try {
            String sql = "UPDATE ";
            sql += table+" ";
            sql += "SET ";
            String criteriaQuery = "";
            for (String key: criteria.keySet()){
                if (!"".equals(criteriaQuery)){
                    criteriaQuery += " , ";
                }
                criteriaQuery += key+"='"+criteria.get(key)+"'";
            }
            sql += criteriaQuery;
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst = db.getConnection().prepareStatement(sql);
            pst.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Data error");
        }
        return result;
    }
    
    public int getRandomNumber(int min, int max){
        Random rd = new Random();
        int number = rd.nextInt(max) + min;
        return number;
    }
    
    public void setFullscreen(JFrame frame){
        frame.setExtendedState( frame.getExtendedState()|JFrame.MAXIMIZED_BOTH );
    }
    
    public void setScrollSpeed(JScrollPane panel,int speed){
        panel.getVerticalScrollBar().setUnitIncrement(speed);
        panel.getHorizontalScrollBar().setUnitIncrement(speed);
    }
    
    public String getCurrentTimeStamp(){
        DateFormat hour = new SimpleDateFormat("HH");
        DateFormat minute = new SimpleDateFormat("mm");
        DateFormat second = new SimpleDateFormat("ss");
        Date date = new Date();
        String result = hour.format(date)+":"+minute.format(date)+":"+second.format(date);
        return result;
    }
}
