package indotraindisp.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TCIPRO
 */


public class constants {
    public static Map<String, String> Constants = new HashMap<String, String>();
    
    public constants(){
        Constants.put("rank_1","Student");
        Constants.put("rank_2","Advanced Student");
        Constants.put("rank_3","Senior Student");
        Constants.put("rank_4","Entry Dispatcher");
        Constants.put("rank_5","Junior Dispatcher");
        Constants.put("rank_6","Advanced Dispatcher");
        Constants.put("rank_7","Senior Distpacher");
        Constants.put("rank_8","Trainer");
        Constants.put("rank_9","Senior Trainer");
        Constants.put("rank_10","Master");
        Constants.put("rank_1_max_score","100");
        Constants.put("rank_2_min_score","100");
        Constants.put("rank_3_min_score","300");
        Constants.put("rank_4_min_score","600");
        Constants.put("rank_5_min_score","900");
        Constants.put("rank_6_min_score","1200");
        Constants.put("rank_7_min_score","1500");
        Constants.put("rank_8_min_score","2000");
        Constants.put("rank_9_min_score","2500");
        Constants.put("rank_10_min_score","3000");
    }
}
