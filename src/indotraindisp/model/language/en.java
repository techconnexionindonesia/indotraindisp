package indotraindisp.model.language;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TCIPRO
 */


public class en {
    public static Map<String, String> text = new HashMap<String, String>();
    
    public en(){
        text.put("name","Name");
        text.put("hour","Hour");
        text.put("minute","Minute");
        text.put("score","Score");
        text.put("total_hours","Total Hours");
        text.put("last_work","Last Work");
        text.put("start","Start");
        text.put("about","About");
        text.put("back","Back");
        text.put("open_web","Open Website");
        text.put("contact_facebook","Contact Facebook");
        text.put("contact_instagram","Contact Instagram");
        text.put("rank_information","By getting score continously from playing ITD, you will get rank increase according to minimum rank score criteria");
        text.put("about_information", "Indonesia Train Dispatcher is a simulation game that simulates how train dispatcher works. In this game, you will work to organize the train journey and ensure the safety of its journey from entering and leaving your service area. <br><br> Indonesia Train Dispatcher has many features, such as realistic background sound that makes you feel like a real station. There are many more features such as phone features, train information features and more. Using the original schedule according to the station will make your playing experience even cooler. Don't forget to get as many scores as possible to get a higher ranking. Indonesia Train Dispatcher is part of the Integrated Transport Simulation System (ITSS) which is an integrated transportation simulation organization. With ITSS, you will be able to experience playing a multiplayer and real time transportation simulation. Currently ITSS is still under development to create an integrated simulation system such as the PPKA simulation which is interconnected between stations played by real players. <br> <br> For more information and updates, please visit the TCI website by pressing the open site button or pressing the button contact");
        text.put("options","Options");
        text.put("reset_game","Reset Game");
        text.put("main_menu","Main Menu");
        text.put("exit","Exit");
        text.put("help","Help");
        text.put("check_update_itd","Check ITD Update");
        text.put("check_update_cta","Check Citayam layout Update");
        text.put("time","Time");
        text.put("play_time","Play Time");
        text.put("info_layout_click","* Click layout to start game");
        text.put("info_layout_buy","* You can buy layout DLC in TCI Website to play ITD");
    }
}
