package indotraindisp.model.language;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TCIPRO
 */


public class id {
    public static Map<String, String> text = new HashMap<String, String>();
    
    public id(){
        text.put("name","Nama");
        text.put("hour","Jam");
        text.put("minute","Menit");
        text.put("score","Skor");
        text.put("total_hours","Total Jam Dinas");
        text.put("last_work","Dinas Terakhir");
        text.put("start","Mulai");
        text.put("about","Tentang");
        text.put("back","Kembali");
        text.put("open_web","Buka Website");
        text.put("contact_facebook","Kontak Facebook");
        text.put("contact_instagram","Kontak Instagram");
        text.put("rank_information","Dengan mendapatkan score secara terus menerus dengan bermain di ITD, maka anda akan mendapatkan kenaikan pangkat sesuai dengan jumlah score minimal masing masing peringkat");
        text.put("about_information","Indonesia Train Dispatcher adalah game simulasi yang mensimulasikan cara kerja PPKA (Pengatur Perjalanan Kereta Api). Dalam game ini, anda akan bekerja untuk mengatur perjalanan kereta api dan memastikan keamanan perjalananya dari masuk sampai keluar wilayah layanan anda.<br><br> Indonesia Train Dispatcher memiliki banyak sekali fitur, seperti background suara yang realistis yang membuat anda berada di seperti di stasiun sebenarnya. Banyak lagi fitur seperti fitur telepon, fitur informasi kereta, dan lainya. Dengan menggunakan jadwal asli sesuai di stasiun akan membuat pengalaman bermain anda semakin keren. Jangan lupa untuk meraih score sebanyak banyaknya untuk mendapatkan peringkat lebih tinggi.<br><br> Indonesia Train Dispatcher merupakan bagian dari Intergrated Transport Simulation System (ITSS) yang merupakan organisasi simulasi transportasi yang terintergrasi. Dengan ITSS, anda akan bisa merasakan bermain simulasi transportasi secara multiplayer dan real time. Saat ini ITSS masih dalam pengembangan untuk menciptakan sistem simulasi yang terintegrasi seperti simulasi PPKA yang saling terhubung antar stasiun yang dimainkan oleh pemain asli.<br><br> Untuk informasi dan update lebih lanjut silahkan kunjungi web TCI dengan menekan tombol buka situs atau menekan tombol kontak");
        text.put("options","Pilihan");
        text.put("reset_game","Reset Game");
        text.put("main_menu","Main Menu");
        text.put("exit","Keluar");
        text.put("help","Bantuan");
        text.put("check_update_itd","Cek Update ITD");
        text.put("check_update_cta","Cek Update Layout Citayam");
        text.put("time","Waktu");
        text.put("play_time","Waktu Berdinas");
        text.put("info_layout_click","* Klik layout memulai game");
        text.put("info_layout_buy","* Anda bisa membeli layout yang tersedia untuk memainkan ITD di website TCI");
    }
}
